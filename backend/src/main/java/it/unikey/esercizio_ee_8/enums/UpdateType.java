package it.unikey.esercizio_ee_8.enums;

public enum UpdateType {
    PUT,
    PATCH
}
