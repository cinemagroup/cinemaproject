package it.unikey.esercizio_ee_8.repositories;

import it.unikey.esercizio_ee_8.entities.ActorManagerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ActorManagerRepository extends JpaRepository<ActorManagerEntity, Long> {

   /* Optional<ActorManagerEntity> findByActorManagerEmail(String email);*/

    Optional<ActorManagerEntity> findByActorManagerEmailAndActorManagerPassword(String email, String password);
}
