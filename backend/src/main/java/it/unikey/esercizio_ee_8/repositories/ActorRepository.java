package it.unikey.esercizio_ee_8.repositories;

import it.unikey.esercizio_ee_8.entities.ActorEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActorRepository extends JpaRepository<ActorEntity, Long> {

}
