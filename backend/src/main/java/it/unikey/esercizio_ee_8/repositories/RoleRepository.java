package it.unikey.esercizio_ee_8.repositories;

import it.unikey.esercizio_ee_8.entities.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<RoleEntity, Long> {
}
