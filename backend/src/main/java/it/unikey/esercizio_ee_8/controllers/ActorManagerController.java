package it.unikey.esercizio_ee_8.controllers;

import it.unikey.esercizio_ee_8.dto.ActorManager.ActorManagerDTO;
import it.unikey.esercizio_ee_8.dto.ActorManager.ManagerResponseDTO;
import it.unikey.esercizio_ee_8.enums.UpdateType;
import it.unikey.esercizio_ee_8.exceptions.ActorManager.ActorManagerException;
import it.unikey.esercizio_ee_8.exceptions.ActorManager.ManagerNotFoundException;
import it.unikey.esercizio_ee_8.interfaces.MyCRUD;
import it.unikey.esercizio_ee_8.interfaces.MyErrorStatusResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@CrossOrigin
@RequestMapping("/actorManagers")
@Slf4j
public class ActorManagerController implements MyErrorStatusResponse<ManagerResponseDTO> {

    private MyCRUD<ActorManagerDTO> actorManagerService;

    @Autowired
    public ActorManagerController(MyCRUD<ActorManagerDTO> actorManagerService) {
        this.actorManagerService = actorManagerService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<ManagerResponseDTO> getManagerById(@PathVariable int id) {
       return checkAndGetManagerOrException(id);
    }

    private ResponseEntity<ManagerResponseDTO> checkAndGetManagerOrException(@PathVariable int id) {
        try {
            return checkAndGetManager(id);
        } catch (ManagerNotFoundException ex) {
            return returnErrorStatusResponse(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    private ResponseEntity<ManagerResponseDTO> checkAndGetManager(@PathVariable int id) {
        ActorManagerDTO manager = actorManagerService.getById(id);
        return ResponseEntity.ok().body(new ManagerResponseDTO(manager));
    }

    @GetMapping
    public ResponseEntity<Set<ActorManagerDTO>> getAllManagers() {

        Set<ActorManagerDTO> managers = actorManagerService.getAll();

        return ResponseEntity.ok().body(managers);
    }

    //TODO sistemare questo post che corrisponde alla login
    /*@PostMapping("/login")
    public ResponseEntity<ManagerResponseDTO> getManagerByEmailAndPassword(String actorManagerEmail, String actorManagerPassword) {
        log.info("try to get actor of email and password" + actorManagerEmail +  actorManagerPassword);
        return checkAndGetManagerOrException2(actorManagerEmail, actorManagerPassword);
    }

    private ResponseEntity<ManagerResponseDTO> checkAndGetManagerOrException2(String actorManagerEmail, String actorManagerPassword) {
        try {
            return checkAndGetManager2(actorManagerEmail, actorManagerPassword);
        } catch (ManagerNotFoundException ex) {
            return returnErrorStatusResponse(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    private ResponseEntity<ManagerResponseDTO> checkAndGetManager2(String actorManagerEmail, String actorManagerPassword) {
        ActorManagerDTO manager = actorManagerService.getByEmailAndPassword(actorManagerEmail, actorManagerPassword);
        return ResponseEntity.ok().body(new ManagerResponseDTO(manager));
    }*/
    //TODO fine della parte da sistemare

    //TODO sistemare questo get che corrisponde alla login
    @GetMapping("/login/{actorManagerEmail}/{actorManagerPassword}")
    public ResponseEntity<ManagerResponseDTO> getManagerByEmailAndPassword(@PathVariable String actorManagerEmail, @PathVariable String actorManagerPassword) {
        /*log.info("try to get actor of email and password" + actorManagerEmail +  actorManagerPassword);*/
        return checkAndGetManagerOrException2(actorManagerEmail, actorManagerPassword);
    }

    private ResponseEntity<ManagerResponseDTO> checkAndGetManagerOrException2(@PathVariable String actorManagerEmail, @PathVariable String actorManagerPassword) {
        try {
            return checkAndGetManager2(actorManagerEmail, actorManagerPassword);
        } catch (ManagerNotFoundException ex) {
            return returnErrorStatusResponse(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    private ResponseEntity<ManagerResponseDTO> checkAndGetManager2(@PathVariable String actorManagerEmail, @PathVariable String actorManagerPassword) {
        ActorManagerDTO manager = actorManagerService.getByEmailAndPassword(actorManagerEmail, actorManagerPassword);
        return ResponseEntity.ok().body(new ManagerResponseDTO(manager));
    }
    //TODO fine della parte da sistemare

    @PostMapping
    public ResponseEntity<ManagerResponseDTO> insertManager(@RequestBody ActorManagerDTO actorManagerDTO) {
        return checkAndInsertManagerOrException(actorManagerDTO);
    }

    private ResponseEntity<ManagerResponseDTO> checkAndInsertManagerOrException(@RequestBody ActorManagerDTO actorManagerDTO) {
        try {
            return checkAndInsertManager(actorManagerDTO);
        } catch (ActorManagerException ex) {
            return returnErrorStatusResponse(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    private ResponseEntity<ManagerResponseDTO> checkAndInsertManager(@RequestBody ActorManagerDTO actorManagerDTO) {
        // Così facendo, salvo in actorManagerDTO il nuovo id, e lo mostro
        actorManagerDTO = actorManagerService.insert(actorManagerDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(new ManagerResponseDTO(actorManagerDTO));
    }

    @PutMapping("/{id}")
    public ResponseEntity<ManagerResponseDTO> updateManagerWithPut(@PathVariable int id,
                                                                   @RequestBody ActorManagerDTO actorManagerDTO) {

        actorManagerDTO.setActorManagerId(id);
        return updateWithPutOrPatch(actorManagerDTO, UpdateType.PUT);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<ManagerResponseDTO> updateManagerWithPatch(@PathVariable int id,
                                                                     @RequestBody ActorManagerDTO actorManagerDTO) {

        actorManagerDTO.setActorManagerId(id);
        return updateWithPutOrPatch(actorManagerDTO, UpdateType.PATCH);
    }

    private ResponseEntity<ManagerResponseDTO> updateWithPutOrPatch(ActorManagerDTO actorManagerDTO, UpdateType updateType) {
        try {
            actorManagerDTO = actorManagerService.update(actorManagerDTO, updateType);
            return ResponseEntity.status(HttpStatus.CREATED).body(new ManagerResponseDTO(actorManagerDTO));
        } catch (ManagerNotFoundException ex) {
            return returnErrorStatusResponse(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @PostMapping("/{id}")
    public ResponseEntity<ManagerResponseDTO> deleteManagerById(@PathVariable int id) {
        return checkAndDeleteManagerOrException(id);
    }

    private ResponseEntity<ManagerResponseDTO> checkAndDeleteManagerOrException(@PathVariable int id) {
        try {
            return checkAndDeleteManager(id);
        } catch (ManagerNotFoundException ex) {
            return returnErrorStatusResponse(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    private ResponseEntity<ManagerResponseDTO> checkAndDeleteManager(@PathVariable int id) {
        actorManagerService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @Override
    public ResponseEntity<ManagerResponseDTO> returnErrorStatusResponse(HttpStatus httpStatus, String errorMessage) {
        log.warn(errorMessage);
        return ResponseEntity.status(httpStatus)
                .body(new ManagerResponseDTO(errorMessage));
    }
}
