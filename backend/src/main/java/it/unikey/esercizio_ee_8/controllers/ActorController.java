package it.unikey.esercizio_ee_8.controllers;

import it.unikey.esercizio_ee_8.dto.Actor.ActorDTO;
import it.unikey.esercizio_ee_8.dto.Actor.ActorResponseDTO;
import it.unikey.esercizio_ee_8.enums.UpdateType;
import it.unikey.esercizio_ee_8.exceptions.Actor.ActorException;
import it.unikey.esercizio_ee_8.exceptions.Actor.ActorNotFoundException;
import it.unikey.esercizio_ee_8.exceptions.ActorManager.ManagerNotFoundException;
import it.unikey.esercizio_ee_8.interfaces.MyCRUD;
import it.unikey.esercizio_ee_8.interfaces.MyErrorStatusResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@CrossOrigin
@RequestMapping("/actors")
@Slf4j
public class ActorController implements MyErrorStatusResponse<ActorResponseDTO> {

    private MyCRUD<ActorDTO> actorService;

    @Autowired
    public ActorController(MyCRUD<ActorDTO> actorService) {
        this.actorService = actorService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<ActorResponseDTO> getActorById(@PathVariable int id) {
        return checkAndGetActorOrException(id);
    }

    private ResponseEntity<ActorResponseDTO> checkAndGetActorOrException(@PathVariable int id) {
        try {
            return checkAndGetActor(id);
        } catch (ActorNotFoundException ex) {
            return returnErrorStatusResponse(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    private ResponseEntity<ActorResponseDTO> checkAndGetActor(@PathVariable int id) {
        ActorDTO actor = actorService.getById(id);
        return ResponseEntity.ok().body(new ActorResponseDTO(actor));
    }

    @GetMapping
    public ResponseEntity<Set<ActorDTO>> getAllActors() {

        Set<ActorDTO> actors = actorService.getAll();

        return ResponseEntity.ok().body(actors);
    }

    @PostMapping
    public ResponseEntity<ActorResponseDTO> insertActor(@RequestBody ActorDTO actorDTO) {
        return checkAndInsertActorOrException(actorDTO);
    }

    private ResponseEntity<ActorResponseDTO> checkAndInsertActorOrException(@RequestBody ActorDTO actorDTO) {
        try {
            return checkAndInsertActor(actorDTO);
        } catch (ActorException ex) {
            return returnErrorStatusResponse(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    private ResponseEntity<ActorResponseDTO> checkAndInsertActor(@RequestBody ActorDTO actorDTO) {
        actorDTO = actorService.insert(actorDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(new ActorResponseDTO(actorDTO));
    }

    @PutMapping("/{id}")
    public ResponseEntity<ActorResponseDTO> updateActorWithPut(@PathVariable int id,
                                                               @RequestBody ActorDTO actorDTO) {

        actorDTO.setActorId(id);
        return updateWithPutOrPatch(actorDTO, UpdateType.PUT);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<ActorResponseDTO> updateActorWithPatch(@PathVariable int id,
                                                                 @RequestBody ActorDTO actorDTO) {

        actorDTO.setActorId(id);
        return updateWithPutOrPatch(actorDTO, UpdateType.PATCH);
    }

    private ResponseEntity<ActorResponseDTO> updateWithPutOrPatch(ActorDTO actorDTO, UpdateType updateType) {
        try {
            actorDTO = actorService.update(actorDTO, updateType);
            return ResponseEntity.status(HttpStatus.CREATED).body(new ActorResponseDTO(actorDTO));
        } catch (ActorNotFoundException ex) {
            return returnErrorStatusResponse(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @PostMapping("/{id}")
    public ResponseEntity<ActorResponseDTO> deleteActorById(@PathVariable int id) {
        return checkAndDeleteActorOrException(id);
    }

    private ResponseEntity<ActorResponseDTO> checkAndDeleteActorOrException(@PathVariable int id) {
        try {
            return checkAndDeleteActor(id);
        } catch (ManagerNotFoundException ex) {
            return returnErrorStatusResponse(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    private ResponseEntity<ActorResponseDTO> checkAndDeleteActor(@PathVariable int id) {
        actorService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @Override
    public ResponseEntity<ActorResponseDTO> returnErrorStatusResponse(HttpStatus httpStatus, String errorMessage) {
        log.warn(errorMessage);
        return ResponseEntity.status(httpStatus)
                .body(new ActorResponseDTO(errorMessage));
    }
}
