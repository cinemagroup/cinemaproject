package it.unikey.esercizio_ee_8.mappers;

import it.unikey.esercizio_ee_8.dto.Actor.ActorDTO;
import it.unikey.esercizio_ee_8.dto.ActorManager.ActorManagerDTO;
import it.unikey.esercizio_ee_8.dto.RoleDTO;
import it.unikey.esercizio_ee_8.entities.ActorEntity;
import it.unikey.esercizio_ee_8.entities.ActorManagerEntity;
import it.unikey.esercizio_ee_8.entities.RoleEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.Set;

@Mapper(componentModel = "spring")
public interface ActorMapper {

    ActorDTO toActorDTO(ActorEntity actorEntity);
    ActorEntity toActorEntity(ActorDTO actorDTO);
    Set<ActorDTO> toSetActorDTO(Set<ActorEntity> actors);

    @Mappings(
            @Mapping(target = "actors", ignore = true)
    )
    ActorManagerDTO toActorManagerDTO(ActorManagerEntity manager);

    @Mappings(
            @Mapping(target = "actorManagers", ignore = true)
    )
    RoleDTO toRoleDTO(RoleEntity role);
}
