package it.unikey.esercizio_ee_8;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class EsercizioEe8Application {

    public static void main(String[] args) {
        SpringApplication.run(EsercizioEe8Application.class, args);
    }

}
