package it.unikey.esercizio_ee_8.services;

import it.unikey.esercizio_ee_8.dto.ActorManager.ActorManagerDTO;
import it.unikey.esercizio_ee_8.entities.ActorManagerEntity;
import it.unikey.esercizio_ee_8.enums.UpdateType;
import it.unikey.esercizio_ee_8.exceptions.ActorManager.ActorManagerException;
import it.unikey.esercizio_ee_8.exceptions.ActorManager.ManagerNotFoundException;
import it.unikey.esercizio_ee_8.interfaces.MyCRUD;
import it.unikey.esercizio_ee_8.mappers.ActorManagerMapper;
import it.unikey.esercizio_ee_8.repositories.ActorManagerRepository;
import org.springframework.beans.factory.annotation.Autowired;
/*import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;*/
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.LinkedHashSet;
import java.util.Set;

@Service
public class ActorManagerService implements MyCRUD<ActorManagerDTO>/*, UserDetailsService*/ {

    private ActorManagerRepository actorManagerRepository;
    private ActorManagerMapper actorManagerMapper;

    @Autowired
    public ActorManagerService(ActorManagerRepository actorManagerRepository, ActorManagerMapper actorManagerMapper) {
        this.actorManagerRepository = actorManagerRepository;
        this.actorManagerMapper = actorManagerMapper;
    }

    @Override
    public ActorManagerDTO getById(int id) {

        ActorManagerEntity manager = actorManagerRepository.findById((long) id)
                .orElseThrow(() -> new ManagerNotFoundException("No manager with an id equals to " + id));

        return actorManagerMapper.toActorManagerDTO(manager);
    }

    public ActorManagerDTO getByEmailAndPassword(String email, String password) {

        ActorManagerEntity manager = actorManagerRepository.findByActorManagerEmailAndActorManagerPassword(email, password)
                .orElseThrow(() -> new ManagerNotFoundException("Incorrect email or password"));

        return actorManagerMapper.toActorManagerDTO(manager);
    }

    @Override
    public Set<ActorManagerDTO> getAll() {

        Set<ActorManagerEntity> managers = new LinkedHashSet<>(actorManagerRepository.findAll());

        return actorManagerMapper.toSetActorManagerDTO(managers);
    }

    @Override
    @Transactional
    public ActorManagerDTO insert(ActorManagerDTO actorManagerDTO) {

        ActorManagerEntity manager = actorManagerMapper.toActorManagerEntity(actorManagerDTO);

        if (actorManagerDTO.getActorManagerId() == 0) {
            // salvando il contenuto del save() dentro manager, ottengo anche il nuovo id del manager
            manager = actorManagerRepository.save(manager);
            return actorManagerMapper.toActorManagerDTO(manager);
        }
        throw new ActorManagerException();
    }

    @Override
    @Transactional
    public ActorManagerDTO update(ActorManagerDTO actorManagerDTO, UpdateType updateType) {

        updateFactory(actorManagerDTO, updateType);
        ActorManagerEntity manager = actorManagerMapper.toActorManagerEntity(actorManagerDTO);
        manager = actorManagerRepository.save(manager);
        return actorManagerMapper.toActorManagerDTO(manager);
    }

    private void updateFactory(ActorManagerDTO actorManagerDTO, UpdateType updateType) {

        switch (updateType) {
            case PUT:
                /*ActorManagerEntity actorManagerEntity =*/ actorManagerRepository.findById(actorManagerDTO.getActorManagerId())
                        .orElseThrow(() -> new ManagerNotFoundException("No manager with an id equals to " + actorManagerDTO.getActorManagerId()));
                break;
            case PATCH:
                copyFieldIfNull(actorManagerDTO);
                break;
        }
    }

    private void copyFieldIfNull(ActorManagerDTO actorManagerDTO) {

        ActorManagerEntity actorManagerEntity = actorManagerRepository.findById(actorManagerDTO.getActorManagerId())
                .orElseThrow(() -> new ManagerNotFoundException("No manager with an id equals to " + actorManagerDTO.getActorManagerId()));

        if (actorManagerDTO.getActorManagerName() == null)
            actorManagerDTO.setActorManagerName(actorManagerEntity.getActorManagerName());
        if (actorManagerDTO.getActorManagerLastname() == null)
            actorManagerDTO.setActorManagerLastname(actorManagerEntity.getActorManagerLastname());
        if (actorManagerDTO.getRole() == null)
            actorManagerDTO.setRole(actorManagerMapper.toRoleDTO(actorManagerEntity.getRole()));
        if (actorManagerDTO.getActorManagerEmail() == null)
            actorManagerDTO.setActorManagerEmail(actorManagerEntity.getActorManagerEmail());
        if (actorManagerDTO.getActorManagerPassword() == null)
            actorManagerDTO.setActorManagerPassword(actorManagerEntity.getActorManagerPassword());
    }

    @Override
    @Transactional
    public void delete(int id) {

        ActorManagerEntity manager = actorManagerRepository.findById((long) id)
                .orElseThrow(() -> new ManagerNotFoundException("No manager with an id equals to " + id));

        actorManagerRepository.deleteById(manager.getActorManagerId());
    }

   /* @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        ActorManagerEntity manager = actorManagerRepository.findByActorManagerEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException("Wrong credentials"));

        return null;
    }*/
}
