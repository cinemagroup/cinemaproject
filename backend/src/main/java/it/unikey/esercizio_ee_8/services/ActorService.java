package it.unikey.esercizio_ee_8.services;

import it.unikey.esercizio_ee_8.dto.Actor.ActorDTO;
import it.unikey.esercizio_ee_8.entities.ActorEntity;
import it.unikey.esercizio_ee_8.enums.UpdateType;
import it.unikey.esercizio_ee_8.exceptions.Actor.ActorException;
import it.unikey.esercizio_ee_8.exceptions.Actor.ActorNotFoundException;
import it.unikey.esercizio_ee_8.interfaces.MyCRUD;
import it.unikey.esercizio_ee_8.mappers.ActorManagerMapper;
import it.unikey.esercizio_ee_8.mappers.ActorMapper;
import it.unikey.esercizio_ee_8.repositories.ActorManagerRepository;
import it.unikey.esercizio_ee_8.repositories.ActorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.LinkedHashSet;
import java.util.Set;

@Service
public class ActorService implements MyCRUD<ActorDTO> {

    private ActorRepository actorRepository;
    private ActorMapper actorMapper;
    private ActorManagerRepository actorManagerRepository;
    private ActorManagerMapper actorManagerMapper;

    @Autowired
    public ActorService(ActorRepository actorRepository,
                        ActorMapper actorMapper,
                        ActorManagerRepository actorManagerRepository,
                        ActorManagerMapper actorManagerMapper) {
        this.actorRepository = actorRepository;
        this.actorMapper = actorMapper;
        this.actorManagerRepository = actorManagerRepository;
        this.actorManagerMapper = actorManagerMapper;
    }

    @Override
    public ActorDTO getById(int id) {

        ActorEntity actor = actorRepository.findById((long) id)
                .orElseThrow(() -> new ActorNotFoundException("No actor with an id equals to " + id));

        return actorMapper.toActorDTO(actor);
    }

    @Override
    public ActorDTO getByEmailAndPassword(String email, String password) {
        return null;
    }

    @Override
    public Set<ActorDTO> getAll() {

        Set<ActorEntity> actors = new LinkedHashSet<>(actorRepository.findAll());

        return actorMapper.toSetActorDTO(actors);
    }

    @Override
    public ActorDTO insert(ActorDTO actorDTO) {

        ActorEntity actor = actorMapper.toActorEntity(actorDTO);

        //TODO le righe di codice commentate che seguono sono state risolte con un cascade merge
        //TODO inserito nella relazione manytoone di actorentity
        /*ActorManagerEntity manager = actorManagerMapper.toActorManagerEntity(actorDTO.getActorManager());*/

        /*if (actorDTO.getActorManager().getActorManagerId() == 0) {
            manager = actorManagerRepository.save(manager);
        }
        if (actorDTO.getActorManager() == null) {
            manager.setRole(null);
            manager.setActors(null);
        }
        actor.setActorManager(manager);*/

        if (actorDTO.getActorId() == 0) {
            actor = actorRepository.save(actor);
            return actorMapper.toActorDTO(actor);
        }
        throw new ActorException();
    }

    @Override
    @Transactional
    public ActorDTO update(ActorDTO actorDTO, UpdateType updateType) {

        updateFactory(actorDTO, updateType);
        ActorEntity actor = actorMapper.toActorEntity(actorDTO);
        actor = actorRepository.save(actor);
        return actorMapper.toActorDTO(actor);
    }

    private void updateFactory(ActorDTO actorDTO, UpdateType updateType) {

        switch (updateType) {
            case PUT:
                /*ActorManagerEntity actorManagerEntity =*/ actorRepository.findById(actorDTO.getActorId())
                    .orElseThrow(() -> new ActorNotFoundException("No actor with an id equals to " + actorDTO.getActorId()));
                break;
            case PATCH:
                copyFieldIfNull(actorDTO);
                break;
        }
    }

    private void copyFieldIfNull(ActorDTO actorDTO) {

        ActorEntity actorEntity = actorRepository.findById(actorDTO.getActorId())
                .orElseThrow(() -> new ActorNotFoundException("No actor with an id equals to " + actorDTO.getActorId()));

        if (actorDTO.getActorName() == null)
            actorDTO.setActorName(actorEntity.getActorName());
        if (actorDTO.getActorSurname() == null)
            actorDTO.setActorSurname(actorEntity.getActorSurname());
        if (actorDTO.getActorPayday() == 0)
            actorDTO.setActorPayday(actorEntity.getActorPayday());
        if (actorDTO.getActorEmail() == null)
            actorDTO.setActorEmail(actorEntity.getActorEmail());
        if (actorDTO.getActorPassword() == null)
            actorDTO.setActorPassword(actorEntity.getActorPassword());
        if (actorDTO.getActorManager() == null)
            actorDTO.setActorManager(actorMapper.toActorManagerDTO(actorEntity.getActorManager()));
    }

    @Override
    public void delete(int id) {
        ActorEntity actor = actorRepository.findById((long) id)
                .orElseThrow(() -> new ActorNotFoundException("No actor with an id equals to " + id));

        actorRepository.deleteById(actor.getActorId());
    }
}
