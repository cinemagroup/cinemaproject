package it.unikey.esercizio_ee_8.entities;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "ACTOR_MANAGER", schema = "dbo")
public class ActorManagerEntity {
    private long actorManagerId;
    private String actorManagerName;
    private String actorManagerLastname;
    private String actorManagerEmail;
    private String actorManagerPassword;
    private Set<ActorEntity> actors;
    private RoleEntity role;

    @Id
    @Column(name = "ACTOR_MANAGER_ID", nullable = false, precision = 0)
    @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    /*@GeneratedValue(generator = "ISEQ$$_73976")*/
    /*@SequenceGenerator(name = "ISEQ$$_73976", sequenceName = "ISEQ$$_73976", allocationSize = 1)*/
    public long getActorManagerId() {
        return actorManagerId;
    }

    public void setActorManagerId(long actorManagerId) {
        this.actorManagerId = actorManagerId;
    }

    @Basic
    @Column(name = "ACTOR_MANAGER_NAME", nullable = false, length = 100)
    public String getActorManagerName() {
        return actorManagerName;
    }

    public void setActorManagerName(String actorManagerName) {
        this.actorManagerName = actorManagerName;
    }

    @Basic
    @Column(name = "ACTOR_MANAGER_LASTNAME", nullable = false, length = 100)
    public String getActorManagerLastname() {
        return actorManagerLastname;
    }

    public void setActorManagerLastname(String actorManagerLastname) {
        this.actorManagerLastname = actorManagerLastname;
    }

    @Basic
    @Column(name = "ACTOR_MANAGER_EMAIL", nullable = false, length = 100)
    public String getActorManagerEmail() {
        return actorManagerEmail;
    }

    public void setActorManagerEmail(String actorManagerEmail) {
        this.actorManagerEmail = actorManagerEmail;
    }

    @Basic
    @Column(name = "ACTOR_MANAGER_PASSWORD", nullable = false, length = 100)
    public String getActorManagerPassword() {
        return actorManagerPassword;
    }

    public void setActorManagerPassword(String actorManagerPassword) {
        this.actorManagerPassword = actorManagerPassword;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActorManagerEntity that = (ActorManagerEntity) o;
        return actorManagerId == that.actorManagerId && Objects.equals(actorManagerName, that.actorManagerName) && Objects.equals(actorManagerLastname, that.actorManagerLastname) && Objects.equals(actorManagerEmail, that.actorManagerEmail) && Objects.equals(actorManagerPassword, that.actorManagerPassword);
    }

    @Override
    public int hashCode() {
        return Objects.hash(actorManagerId, actorManagerName, actorManagerLastname, actorManagerEmail, actorManagerPassword);
    }

    @OneToMany(mappedBy = "actorManager", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    public Set<ActorEntity> getActors() {
        return actors;
    }

    public void setActors(Set<ActorEntity> actors) {
        this.actors = actors;
    }

    @ManyToOne
    @JoinColumn(name = "ROLE_ID", referencedColumnName = "ROLE_ID")
    public RoleEntity getRole() {
        return role;
    }

    public void setRole(RoleEntity role) {
        this.role = role;
    }
}
