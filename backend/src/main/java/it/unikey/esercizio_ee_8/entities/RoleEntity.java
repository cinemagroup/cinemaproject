package it.unikey.esercizio_ee_8.entities;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "ROLE", schema = "dbo")
public class RoleEntity {
    private long roleId;
    private String roleName;
    private Set<ActorManagerEntity> actorManagers;

    @Id
    @Column(name = "ROLE_ID", nullable = false, precision = 0)
    @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
   /* @GeneratedValue(generator = "ISEQ$$_73973")*/
    /*@SequenceGenerator(name = "ISEQ$$_73973", sequenceName = "ISEQ$$_73973", allocationSize = 1)*/
    public long getRoleId() {
        return roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }

    @Basic
    @Column(name = "ROLE_NAME", nullable = true, length = 100)
    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoleEntity that = (RoleEntity) o;
        return roleId == that.roleId && Objects.equals(roleName, that.roleName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(roleId, roleName);
    }

    @OneToMany(mappedBy = "role", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE/*, CascadeType.PERSIST*/})
    public Set<ActorManagerEntity> getActorManagers() {
        return actorManagers;
    }

    public void setActorManagers(Set<ActorManagerEntity> actorManagers) {
        this.actorManagers = actorManagers;
    }
}
