package it.unikey.esercizio_ee_8.entities;

import javax.persistence.*;
import java.sql.Time;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "ACTOR", schema = "dbo")
public class ActorEntity {
    private long actorId;
    private String actorName;
    private Double actorPayday;
    private String actorSurname;
    private String actorEmail;
    private String actorPassword;
    private LocalDateTime actorBirthdate;
    private ActorManagerEntity actorManager;

    @Id
    @Column(name = "ACTOR_ID", nullable = false, precision = 0)
    /*@GeneratedValue(generator = "ISEQ$$_73979")*/
    @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
   /* @SequenceGenerator(name = "ISEQ$$_73979", sequenceName = "ISEQ$$_73979", allocationSize = 1)*/
    public long getActorId() {
        return actorId;
    }

    public void setActorId(long actorId) {
        this.actorId = actorId;
    }

    @Basic
    @Column(name = "ACTOR_NAME", nullable = true, length = 50)
    public String getActorName() {
        return actorName;
    }

    public void setActorName(String actorName) {
        this.actorName = actorName;
    }

    @Basic
    @Column(name = "ACTOR_PAYDAY", nullable = true, precision = 2)
    public Double getActorPayday() {
        return actorPayday;
    }

    public void setActorPayday(Double actorPayday) {
        this.actorPayday = actorPayday;
    }

    @Basic
    @Column(name = "ACTOR_SURNAME", nullable = true, length = 50)
    public String getActorSurname() {
        return actorSurname;
    }

    public void setActorSurname(String actorSurname) {
        this.actorSurname = actorSurname;
    }

    @Basic
    @Column(name = "ACTOR_EMAIL", nullable = false, length = 100)
    public String getActorEmail() {
        return actorEmail;
    }

    public void setActorEmail(String actorEmail) {
        this.actorEmail = actorEmail;
    }

    @Basic
    @Column(name = "ACTOR_PASSWORD", nullable = true, length = 100)
    public String getActorPassword() {
        return actorPassword;
    }

    public void setActorPassword(String actorPassword) {
        this.actorPassword = actorPassword;
    }

    @Basic
    @Column(name = "ACTOR_BIRTHDATE", nullable = true)
    public LocalDateTime getActorBirthdate() {
        return actorBirthdate;
    }

    public void setActorBirthdate(LocalDateTime actorBirthdate) {
        this.actorBirthdate = actorBirthdate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActorEntity that = (ActorEntity) o;
        return actorId == that.actorId && Objects.equals(actorName, that.actorName) && Objects.equals(actorPayday, that.actorPayday) && Objects.equals(actorSurname, that.actorSurname) && Objects.equals(actorEmail, that.actorEmail) && Objects.equals(actorPassword, that.actorPassword) && Objects.equals(actorBirthdate, that.actorBirthdate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(actorId, actorName, actorPayday, actorSurname, actorEmail, actorPassword, actorBirthdate);
    }

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "ACTOR_MANAGER_ID", referencedColumnName = "ACTOR_MANAGER_ID")
    public ActorManagerEntity getActorManager() {
        return actorManager;
    }

    public void setActorManager(ActorManagerEntity actorManager) {
        this.actorManager = actorManager;
    }
}
