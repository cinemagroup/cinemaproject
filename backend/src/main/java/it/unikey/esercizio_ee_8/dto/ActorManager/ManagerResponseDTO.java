package it.unikey.esercizio_ee_8.dto.ActorManager;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ManagerResponseDTO {

    private ActorManagerDTO manager;
    private String message;

    public ManagerResponseDTO(ActorManagerDTO manager) {
        this.manager = manager;
    }

    public ManagerResponseDTO(String message) {
        this.message = message;
    }
}
