package it.unikey.esercizio_ee_8.dto.ActorManager;

import it.unikey.esercizio_ee_8.dto.Actor.ActorDTO;
import it.unikey.esercizio_ee_8.dto.RoleDTO;
import lombok.*;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ActorManagerDTO {

    private long actorManagerId;
    private String actorManagerName;
    private String actorManagerLastname;
    private String actorManagerEmail;
    private String actorManagerPassword;
    private Set<ActorDTO> actors;
    private RoleDTO role;
}
