package it.unikey.esercizio_ee_8.dto;

import it.unikey.esercizio_ee_8.dto.ActorManager.ActorManagerDTO;
import lombok.*;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RoleDTO {

    private long roleId;
    private String roleName;
    private Set<ActorManagerDTO> actorManagers;
}
