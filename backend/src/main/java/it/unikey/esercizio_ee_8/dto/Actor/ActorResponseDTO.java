package it.unikey.esercizio_ee_8.dto.Actor;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ActorResponseDTO {

    private ActorDTO actor;
    private String message;

    public ActorResponseDTO(ActorDTO actor) {
        this.actor = actor;
    }

    public ActorResponseDTO(String message) {
        this.message = message;
    }
}
