package it.unikey.esercizio_ee_8.dto.Actor;

import it.unikey.esercizio_ee_8.dto.ActorManager.ActorManagerDTO;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ActorDTO {

    private long actorId;
    private String actorName;
    private Double actorPayday;
    private String actorSurname;
    private String actorEmail;
    private String actorPassword;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDateTime actorBirthdate;
    private ActorManagerDTO actorManager;
}
