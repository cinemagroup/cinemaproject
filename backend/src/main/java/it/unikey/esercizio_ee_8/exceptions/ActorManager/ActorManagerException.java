package it.unikey.esercizio_ee_8.exceptions.ActorManager;

public class ActorManagerException extends RuntimeException {

    public ActorManagerException() {
        super("Generic ActorManager exception!");
    }

    public ActorManagerException(String message) {
        super(message);
    }
}
