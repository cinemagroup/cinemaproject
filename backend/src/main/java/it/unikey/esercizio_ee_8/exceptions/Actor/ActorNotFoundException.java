package it.unikey.esercizio_ee_8.exceptions.Actor;

public class ActorNotFoundException extends ActorException {

    public ActorNotFoundException() {
        super("Actor not found!");
    }

    public ActorNotFoundException(String message) {
        super(message);
    }
}
