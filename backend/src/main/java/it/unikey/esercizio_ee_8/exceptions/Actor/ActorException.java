package it.unikey.esercizio_ee_8.exceptions.Actor;

public class ActorException extends RuntimeException {

    public ActorException() {
        super("Generic Actor exception!");
    }

    public ActorException(String message) {
        super(message);
    }
}
