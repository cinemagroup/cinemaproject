package it.unikey.esercizio_ee_8.exceptions.ActorManager;

public class ManagerNotFoundException extends ActorManagerException {

    public ManagerNotFoundException() {
        super("Manager not found!");
    }

    public ManagerNotFoundException(String message) {
        super(message);
    }
}
