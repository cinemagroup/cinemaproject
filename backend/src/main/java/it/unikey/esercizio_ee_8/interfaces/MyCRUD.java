package it.unikey.esercizio_ee_8.interfaces;

import it.unikey.esercizio_ee_8.enums.UpdateType;

import java.util.Set;

public interface MyCRUD<E> {

    E getById(int id);
    E getByEmailAndPassword(String email, String password);
    Set<E> getAll();
    E insert(E element);
    E update(E element, UpdateType updateType);
    void delete(int id);
}