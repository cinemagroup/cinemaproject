package it.unikey.esercizio_ee_8.interfaces;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public interface MyErrorStatusResponse<D> {

    ResponseEntity<D> returnErrorStatusResponse(HttpStatus httpStatus, String errorMessage);
}
