package it.unikey.esercizio_ee_8.mappers;

import it.unikey.esercizio_ee_8.dto.Actor.ActorDTO;
import it.unikey.esercizio_ee_8.dto.ActorManager.ActorManagerDTO;
import it.unikey.esercizio_ee_8.dto.RoleDTO;
import it.unikey.esercizio_ee_8.entities.ActorEntity;
import it.unikey.esercizio_ee_8.entities.ActorManagerEntity;
import it.unikey.esercizio_ee_8.entities.RoleEntity;
import java.util.HashSet;
import java.util.Set;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-05-27T11:18:57+0200",
    comments = "version: 1.4.1.Final, compiler: javac, environment: Java 1.8.0_282 (Amazon.com Inc.)"
)
@Component
public class ActorMapperImpl implements ActorMapper {

    @Override
    public ActorDTO toActorDTO(ActorEntity actorEntity) {
        if ( actorEntity == null ) {
            return null;
        }

        ActorDTO actorDTO = new ActorDTO();

        actorDTO.setActorId( actorEntity.getActorId() );
        actorDTO.setActorName( actorEntity.getActorName() );
        actorDTO.setActorPayday( actorEntity.getActorPayday() );
        actorDTO.setActorSurname( actorEntity.getActorSurname() );
        actorDTO.setActorEmail( actorEntity.getActorEmail() );
        actorDTO.setActorPassword( actorEntity.getActorPassword() );
        actorDTO.setActorBirthdate( actorEntity.getActorBirthdate() );
        actorDTO.setActorManager( toActorManagerDTO( actorEntity.getActorManager() ) );

        return actorDTO;
    }

    @Override
    public ActorEntity toActorEntity(ActorDTO actorDTO) {
        if ( actorDTO == null ) {
            return null;
        }

        ActorEntity actorEntity = new ActorEntity();

        actorEntity.setActorId( actorDTO.getActorId() );
        actorEntity.setActorName( actorDTO.getActorName() );
        actorEntity.setActorPayday( actorDTO.getActorPayday() );
        actorEntity.setActorSurname( actorDTO.getActorSurname() );
        actorEntity.setActorEmail( actorDTO.getActorEmail() );
        actorEntity.setActorPassword( actorDTO.getActorPassword() );
        actorEntity.setActorBirthdate( actorDTO.getActorBirthdate() );
        actorEntity.setActorManager( actorManagerDTOToActorManagerEntity( actorDTO.getActorManager() ) );

        return actorEntity;
    }

    @Override
    public Set<ActorDTO> toSetActorDTO(Set<ActorEntity> actors) {
        if ( actors == null ) {
            return null;
        }

        Set<ActorDTO> set = new HashSet<ActorDTO>( Math.max( (int) ( actors.size() / .75f ) + 1, 16 ) );
        for ( ActorEntity actorEntity : actors ) {
            set.add( toActorDTO( actorEntity ) );
        }

        return set;
    }

    @Override
    public ActorManagerDTO toActorManagerDTO(ActorManagerEntity manager) {
        if ( manager == null ) {
            return null;
        }

        ActorManagerDTO actorManagerDTO = new ActorManagerDTO();

        actorManagerDTO.setActorManagerId( manager.getActorManagerId() );
        actorManagerDTO.setActorManagerName( manager.getActorManagerName() );
        actorManagerDTO.setActorManagerLastname( manager.getActorManagerLastname() );
        actorManagerDTO.setActorManagerEmail( manager.getActorManagerEmail() );
        actorManagerDTO.setActorManagerPassword( manager.getActorManagerPassword() );
        actorManagerDTO.setRole( toRoleDTO( manager.getRole() ) );

        return actorManagerDTO;
    }

    @Override
    public RoleDTO toRoleDTO(RoleEntity role) {
        if ( role == null ) {
            return null;
        }

        RoleDTO roleDTO = new RoleDTO();

        roleDTO.setRoleId( role.getRoleId() );
        roleDTO.setRoleName( role.getRoleName() );

        return roleDTO;
    }

    protected Set<ActorEntity> actorDTOSetToActorEntitySet(Set<ActorDTO> set) {
        if ( set == null ) {
            return null;
        }

        Set<ActorEntity> set1 = new HashSet<ActorEntity>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( ActorDTO actorDTO : set ) {
            set1.add( toActorEntity( actorDTO ) );
        }

        return set1;
    }

    protected Set<ActorManagerEntity> actorManagerDTOSetToActorManagerEntitySet(Set<ActorManagerDTO> set) {
        if ( set == null ) {
            return null;
        }

        Set<ActorManagerEntity> set1 = new HashSet<ActorManagerEntity>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( ActorManagerDTO actorManagerDTO : set ) {
            set1.add( actorManagerDTOToActorManagerEntity( actorManagerDTO ) );
        }

        return set1;
    }

    protected RoleEntity roleDTOToRoleEntity(RoleDTO roleDTO) {
        if ( roleDTO == null ) {
            return null;
        }

        RoleEntity roleEntity = new RoleEntity();

        roleEntity.setRoleId( roleDTO.getRoleId() );
        roleEntity.setRoleName( roleDTO.getRoleName() );
        roleEntity.setActorManagers( actorManagerDTOSetToActorManagerEntitySet( roleDTO.getActorManagers() ) );

        return roleEntity;
    }

    protected ActorManagerEntity actorManagerDTOToActorManagerEntity(ActorManagerDTO actorManagerDTO) {
        if ( actorManagerDTO == null ) {
            return null;
        }

        ActorManagerEntity actorManagerEntity = new ActorManagerEntity();

        actorManagerEntity.setActorManagerId( actorManagerDTO.getActorManagerId() );
        actorManagerEntity.setActorManagerName( actorManagerDTO.getActorManagerName() );
        actorManagerEntity.setActorManagerLastname( actorManagerDTO.getActorManagerLastname() );
        actorManagerEntity.setActorManagerEmail( actorManagerDTO.getActorManagerEmail() );
        actorManagerEntity.setActorManagerPassword( actorManagerDTO.getActorManagerPassword() );
        actorManagerEntity.setActors( actorDTOSetToActorEntitySet( actorManagerDTO.getActors() ) );
        actorManagerEntity.setRole( roleDTOToRoleEntity( actorManagerDTO.getRole() ) );

        return actorManagerEntity;
    }
}
