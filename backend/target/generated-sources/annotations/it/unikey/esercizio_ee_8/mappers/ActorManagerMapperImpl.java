package it.unikey.esercizio_ee_8.mappers;

import it.unikey.esercizio_ee_8.dto.Actor.ActorDTO;
import it.unikey.esercizio_ee_8.dto.ActorManager.ActorManagerDTO;
import it.unikey.esercizio_ee_8.dto.RoleDTO;
import it.unikey.esercizio_ee_8.entities.ActorEntity;
import it.unikey.esercizio_ee_8.entities.ActorManagerEntity;
import it.unikey.esercizio_ee_8.entities.RoleEntity;
import java.util.HashSet;
import java.util.Set;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-05-27T11:18:56+0200",
    comments = "version: 1.4.1.Final, compiler: javac, environment: Java 1.8.0_282 (Amazon.com Inc.)"
)
@Component
public class ActorManagerMapperImpl implements ActorManagerMapper {

    @Override
    public ActorManagerEntity toActorManagerEntity(ActorManagerDTO actorDTO) {
        if ( actorDTO == null ) {
            return null;
        }

        ActorManagerEntity actorManagerEntity = new ActorManagerEntity();

        actorManagerEntity.setActorManagerId( actorDTO.getActorManagerId() );
        actorManagerEntity.setActorManagerName( actorDTO.getActorManagerName() );
        actorManagerEntity.setActorManagerLastname( actorDTO.getActorManagerLastname() );
        actorManagerEntity.setActorManagerEmail( actorDTO.getActorManagerEmail() );
        actorManagerEntity.setActorManagerPassword( actorDTO.getActorManagerPassword() );
        actorManagerEntity.setActors( actorDTOSetToActorEntitySet( actorDTO.getActors() ) );
        actorManagerEntity.setRole( roleDTOToRoleEntity( actorDTO.getRole() ) );

        return actorManagerEntity;
    }

    @Override
    public ActorManagerDTO toActorManagerDTO(ActorManagerEntity actor) {
        if ( actor == null ) {
            return null;
        }

        ActorManagerDTO actorManagerDTO = new ActorManagerDTO();

        actorManagerDTO.setActorManagerId( actor.getActorManagerId() );
        actorManagerDTO.setActorManagerName( actor.getActorManagerName() );
        actorManagerDTO.setActorManagerLastname( actor.getActorManagerLastname() );
        actorManagerDTO.setActorManagerEmail( actor.getActorManagerEmail() );
        actorManagerDTO.setActorManagerPassword( actor.getActorManagerPassword() );
        actorManagerDTO.setActors( toSetActorDTO( actor.getActors() ) );
        actorManagerDTO.setRole( toRoleDTO( actor.getRole() ) );

        return actorManagerDTO;
    }

    @Override
    public Set<ActorManagerDTO> toSetActorManagerDTO(Set<ActorManagerEntity> managers) {
        if ( managers == null ) {
            return null;
        }

        Set<ActorManagerDTO> set = new HashSet<ActorManagerDTO>( Math.max( (int) ( managers.size() / .75f ) + 1, 16 ) );
        for ( ActorManagerEntity actorManagerEntity : managers ) {
            set.add( toActorManagerDTO( actorManagerEntity ) );
        }

        return set;
    }

    @Override
    public RoleDTO toRoleDTO(RoleEntity role) {
        if ( role == null ) {
            return null;
        }

        RoleDTO roleDTO = new RoleDTO();

        roleDTO.setRoleId( role.getRoleId() );
        roleDTO.setRoleName( role.getRoleName() );

        return roleDTO;
    }

    @Override
    public Set<ActorDTO> toSetActorDTO(Set<ActorEntity> actors) {
        if ( actors == null ) {
            return null;
        }

        Set<ActorDTO> set = new HashSet<ActorDTO>( Math.max( (int) ( actors.size() / .75f ) + 1, 16 ) );
        for ( ActorEntity actorEntity : actors ) {
            set.add( toActorDTO( actorEntity ) );
        }

        return set;
    }

    @Override
    public ActorDTO toActorDTO(ActorEntity actor) {
        if ( actor == null ) {
            return null;
        }

        ActorDTO actorDTO = new ActorDTO();

        actorDTO.setActorId( actor.getActorId() );
        actorDTO.setActorName( actor.getActorName() );
        actorDTO.setActorPayday( actor.getActorPayday() );
        actorDTO.setActorSurname( actor.getActorSurname() );
        actorDTO.setActorEmail( actor.getActorEmail() );
        actorDTO.setActorPassword( actor.getActorPassword() );
        actorDTO.setActorBirthdate( actor.getActorBirthdate() );

        return actorDTO;
    }

    protected ActorEntity actorDTOToActorEntity(ActorDTO actorDTO) {
        if ( actorDTO == null ) {
            return null;
        }

        ActorEntity actorEntity = new ActorEntity();

        actorEntity.setActorId( actorDTO.getActorId() );
        actorEntity.setActorName( actorDTO.getActorName() );
        actorEntity.setActorPayday( actorDTO.getActorPayday() );
        actorEntity.setActorSurname( actorDTO.getActorSurname() );
        actorEntity.setActorEmail( actorDTO.getActorEmail() );
        actorEntity.setActorPassword( actorDTO.getActorPassword() );
        actorEntity.setActorBirthdate( actorDTO.getActorBirthdate() );
        actorEntity.setActorManager( toActorManagerEntity( actorDTO.getActorManager() ) );

        return actorEntity;
    }

    protected Set<ActorEntity> actorDTOSetToActorEntitySet(Set<ActorDTO> set) {
        if ( set == null ) {
            return null;
        }

        Set<ActorEntity> set1 = new HashSet<ActorEntity>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( ActorDTO actorDTO : set ) {
            set1.add( actorDTOToActorEntity( actorDTO ) );
        }

        return set1;
    }

    protected Set<ActorManagerEntity> actorManagerDTOSetToActorManagerEntitySet(Set<ActorManagerDTO> set) {
        if ( set == null ) {
            return null;
        }

        Set<ActorManagerEntity> set1 = new HashSet<ActorManagerEntity>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( ActorManagerDTO actorManagerDTO : set ) {
            set1.add( toActorManagerEntity( actorManagerDTO ) );
        }

        return set1;
    }

    protected RoleEntity roleDTOToRoleEntity(RoleDTO roleDTO) {
        if ( roleDTO == null ) {
            return null;
        }

        RoleEntity roleEntity = new RoleEntity();

        roleEntity.setRoleId( roleDTO.getRoleId() );
        roleEntity.setRoleName( roleDTO.getRoleName() );
        roleEntity.setActorManagers( actorManagerDTOSetToActorManagerEntitySet( roleDTO.getActorManagers() ) );

        return roleEntity;
    }
}
