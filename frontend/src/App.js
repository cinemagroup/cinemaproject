import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "./components/layout/header/Header";
import Footer from "./components/layout/footer/Footer";
import { Route } from "react-router";
import { Routes } from "react-router";
import Home from "./components/layout/core/home/Home";
import Login from "./components/layout/core/login/Login";
import Welcome from "./components/layout/core/welcome/Welcome";
import ActorList from "./components/layout/core/list/actor/ActorList";
import ActorManagerList from "./components/layout/core/list/manager/ActorManagerList";
import ActorManagerSignUp from "./components/layout/core/signUp/ActorManagerSignUp";
import ActorSignUp from "./components/layout/core/signUp/ActorSignUp";
import SignUpChoice from "./components/layout/core/signUp/SignUpChoice";
import WelcomeActor from "./components/layout/core/welcome/WelcomeActor";
import WelcomeActorManager from "./components/layout/core/welcome/WelcomeActorManager";


function App() {
    return (
        <div className="App">
            <Header />
            <div id={"core"}>
                <Routes>
                    <Route path={'/'} element={<Home />} />
                    <Route path={'/login'} element={<Login />} />
                    <Route path={'/welcome'} element={<Welcome />} />
                    <Route path={'/signup'} element={<SignUpChoice />}/>
                    <Route path={'/signup/actormanager'} element={<ActorManagerSignUp />} />
                    <Route path={'/signup/actor'} element={<ActorSignUp />} />
                    <Route path={`/actor/welcome/:actorId`} element={<WelcomeActor />}/>
                    <Route path={`/actormanager/welcome/:actorManagerId`} element={<WelcomeActorManager />} />
                    <Route path={'/list/actorlist'} element={<ActorList />} />
                    <Route path={'/list/actormanagerlist'} element={<ActorManagerList />} />
                </Routes>
            </div>
            <Footer />
        </div>
    );
}

export default App;
