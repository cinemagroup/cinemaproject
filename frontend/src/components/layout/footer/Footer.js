import './Footer.css'

const Footer = () => {

    return (
        <div id={"footer"}>
            <p>®Copyright 2021 D'Ottavio-Ghirelli-Pacini, all rights reserved</p>
        </div>
    );
}

export default Footer;