import {NavLink} from "react-router-dom";
import {useState} from "react";

const SignUpChoice = () => {

    const [signUpOption, setSignUpOption] = useState(0)
    let directory = '';

    const handleOption= (e) => {
        setSignUpOption(e.target.value)
        console.log(signUpOption)
    }

    signUpOption === '1'
    ? directory= "actor"
        : directory = "actormanager";




    return (

        <>
        <h1>Hi! How do you wanna sign up?</h1>

            <div onChange={handleOption} name="signUpOption" id="signUpOption">
                <input type="radio"  value='1' name="signUpOption"/> I'm an actor
                <input type="radio"  value='2' name="signUpOption"/> I'm a manager
            </div>
                <NavLink className={'btn btn-primary'} to={`/signup/${directory}`}>
                    <button >Go</button>
                </NavLink>


        </>
    )
}

export default SignUpChoice;

