import './SignUp.css'
import {useState} from "react";
import useFetch from "../../../../services/useFetch";
import {signUpActor} from "../../../../services/signUpActorService";
import emptyNewActorModel from "../../../../utils/dataModels/emptyNewActorModel";
import {NavLink} from "react-router-dom";

const ActorSignUp = () => {

    const [newUserData, setNewUserData] = useState(emptyNewActorModel);
    const {data: actors, error, isLoading} = useFetch('http://34.65.241.99:90/actors')


    async function handleSubmit(event) {
        event.preventDefault();
        const lastId = actors[actors.length-1].id+1;
        setNewUserData((prevState) => {
            return {...prevState, id: lastId}
        })
        console.log("---STO PER INIZIARE IL TRY---")
        try{

            console.log("---sono dentro il try---")
            await signUpActor(newUserData)

        }catch {
            console.error('SUBMIT NON RIUSCITO')
        }
    }

    const handleChange = (e) => {
        setNewUserData((prevNewUserState) => {
            return {...prevNewUserState, [e.target.name]: e.target.value}
        })
    }

    return (
        <>
            <div>
                <form className={'signUpForm'} onSubmit={handleSubmit}>
                    <h1>Inserisci i tuoi dati</h1>
                    <input className={'inputBar'} type="text"
                           placeholder='Name'
                           name={'actorName'}
                           value={newUserData.actorName}
                           onChange={handleChange}
                    />
                    <input className={'inputBar'} type="text"
                           placeholder='Lastname'
                           name={'actorSurname'}
                           value={newUserData.actorSurname}
                           onChange={handleChange}
                    />
                    <input className={'inputBar'} type="text"
                           placeholder='Email'
                           name={'actorEmail'}
                           value={newUserData.actorEmail}
                           onChange={handleChange}
                    />
                    <input className={'inputBar'} type="text"
                           placeholder='Password'
                           name={'actorPassword'}
                           value={newUserData.actorPassword}
                           onChange={handleChange}
                    />
                    <input className={'inputBar'} type="number"
                           placeholder='Payday'
                           name={'actorPayday'}
                           value={newUserData.actorPayday}
                           onChange={handleChange}
                    />
       {             <input className={'inputBar'} type="date"
                           placeholder='Birthdate'
                           name={'actorBirthdate'}
                           value={newUserData.actorBirthdate}
                           onChange={handleChange}
                    />}
                    <button
                        className={'btn btn-success'}
                        type={'submit'}>
                        Sign up
                    </button>
                    <NavLink to={`/signup/actormanager`}>
                        <h6>I'm a manager</h6>
                    </NavLink>
                </form>
            </div>
        </>
    );
}

export default ActorSignUp;
