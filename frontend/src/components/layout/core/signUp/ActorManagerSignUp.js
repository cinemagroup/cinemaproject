import './SignUp.css'
import {useState} from "react";
import useFetch from "../../../../services/useFetch";
import {signUpActorManager} from "../../../../services/signUpActorManagerService";
import emptyNewActorManagerModel from "../../../../utils/dataModels/emptyNewActorManagerModel";
import {NavLink} from "react-router-dom";

const ActorManagerSignUp = () => {

    const [newUserData, setNewUserData] = useState(emptyNewActorManagerModel);
    const {data: actormanagers, error, isLoading} = useFetch('http://34.65.241.99:90/actorManagers')


    async function handleSubmit(event) {
        event.preventDefault();
        const lastId = actormanagers[actormanagers.length-1].id+1;
        setNewUserData((prevState) => {
            return {...prevState, id: lastId}
        })
        console.log("---STO PER INIZIARE IL TRY---")
        try{

            console.log("---sono dentro il try---")
            await signUpActorManager(newUserData)

        }catch {
            console.error('SUBMIT NON RIUSCITO')
        }
    }

    const handleChange = (e) => {
        setNewUserData((prevNewUserState) => {
           return {...prevNewUserState, [e.target.name]: e.target.value}
        })
    }

    return (
      <>
          <div>
              <form className={'signUpForm'} onSubmit={handleSubmit}>
                  <h1>Inserisci i tuoi dati</h1>
                  <input className={'inputBar'} type="text"
                         placeholder='Name'
                         name={'actorManagerName'}
                         value={newUserData.actorManagerName}
                         onChange={handleChange}
                  />
                  <input className={'inputBar'} type="text"
                         placeholder='Lastname'
                         name={'actorManagerLastname'}
                         value={newUserData.actorManagerLastname}
                         onChange={handleChange}
                  />
                  <input className={'inputBar'} type="text"
                         placeholder='Email'
                         name={'actorManagerEmail'}
                         value={newUserData.actorManagerEmail}
                         onChange={handleChange}
                  />
                  <input className={'inputBar'} type="text"
                         placeholder='Password'
                         name={'actorManagerPassword'}
                         value={newUserData.actorManagerPassword}
                         onChange={handleChange}
                  />
                  <button
                      className={'btn btn-success'}
                      type={'submit'}>
                      Sign up
                  </button>
              </form>
              <NavLink to={`/signup/actor`}>
                  <h6>I'm an actor</h6>
              </NavLink>
          </div>
      </>
    );
}

export default ActorManagerSignUp;
