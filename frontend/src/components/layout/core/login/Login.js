import './Login.css'
import {useState} from "react";
import {NavLink} from "react-router-dom";
import useLogin from "../../../../services/useLogin";

const Login = () => {

    const [formValue, setFormValue] = useState({
        username: '',
        password: ''
    });

    const {data: manager, error, isLoading} = useLogin('http://34.65.241.99:90/actorManagers/login', formValue)

    const handleSubmit = (event) => {
        event.preventDefault();
        console.log(`Username: ${formValue.username} | Password: ${formValue.password}`);
    }

    const handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        console.log(name);

        setFormValue({
            ...formValue,
            [name]: value
        })
    }

    return (
        <>
            <div>
                {

                }
                <div>
                    Managerdiv
                    {manager.name}
                </div>
                <form className={'loginForm'} onSubmit={handleSubmit}>
                    <h1>Inserisci il tuo username e la password</h1>
                    <input className={'inputBar'} type="text"
                           placeholder='Username'
                           name={'username'}
                           value={formValue.username}
                           onChange={handleChange}
                    />
                    <input className={'inputBar'} type="text"
                           placeholder='Password'
                           name={'password'}
                           value={formValue.password}
                           onChange={handleChange}
                    />
                    <button
                        className={'btn btn-success'}
                        type={'submit'}>
                        Login
                    </button>
                    <div id={'signUpQuestion'}>
                        <p>Non sei registrato?</p>
                        <NavLink to={'/signup'}>Registrati</NavLink>
                    </div>
                </form>
            </div>
        </>
    );
}

export default Login;
