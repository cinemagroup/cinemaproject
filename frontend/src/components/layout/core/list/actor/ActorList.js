import './ActorList.css'
import useFetch from "../../../../../services/useFetch";

const ActorList = () => {

    const {data: actors, error, isLoading} = useFetch('http://34.65.241.99:90/actors')

    if (error) { return <h1>Errore del server</h1> }
    if (isLoading) { return <h1>Loading...</h1>}
    return (
      <div>
          <div id={'bar'}>
            <h1>Actor List</h1>
          </div>
          <div>
              {
                  actors.map((actor) => {
                      return <div>
                            <div id={'actorInfoAndButton'}>
                                <div className={'picAndNameInfo'}>
                                    <img src={'../../../../utils/static/images/bojack.jpg'} alt="actorImg"/>
                                    <h2>{actor.actorName} {actor.actorSurname}  </h2>
                                </div>
                                <div  class={'buttonInfo'}>
                                    <button className={'btn btn-primary'}>Aggiungi</button>
                                    <button className={'btn btn-primary'}>See profile</button>
                                    <button className={'btn btn-primary'}>Send a message</button>
                                </div>
                            </div>
                            <p>{actor.actorBirthdate}</p>
                            <p>{actor.actorPayday}</p>
                      </div>

                  })
              }
          </div>
      </div>
    );
}

export default ActorList;
