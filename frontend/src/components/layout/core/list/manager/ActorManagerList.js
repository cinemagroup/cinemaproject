import './ActorManagerList.css'
import useFetch from "../../../../../services/useFetch";
import {NavLink} from "react-router-dom";

const ActorManagerList = () => {

        const {data: actorManagers, error, isLoading} = useFetch('http://34.65.241.99:90/actorManagers')

        if (error) {

            return <h1>Errore del server dentro actormanagerlist</h1>
        }

        if (isLoading) {

            return <h1>Loading...</h1>

        }

        return (
            <div>
                <div id={'bar'}>
                    <h1>Actor Manager List</h1>
                </div>
                <div>

                    {
                        actorManagers.map((actorManager) => {

                            return <div id={'managerInfoAndButton'}>
                                <div class={'picAndNameInfo'}>
                                    <img src={'../../../../utils/static/images/princess.jpg'} alt="actorManagerImg"/>
                                    <h3>{actorManager.actorManagerName} {actorManager.actorManagerLastname}</h3>
                                </div>

                                <div class={'buttonInfo'}>
                                    <button className={'btn btn-primary'}>Add</button>
                                    {/*<NavLink to={}>*/}
                                        <button className={'btn btn-primary'}>See profile</button>
                                  {/*  </NavLink>*/}

                                    <button className={'btn btn-primary'}>Send a message</button>
                                </div>

                                        {/*<p>{actorManager.role}</p>*/}


                            </div>

                        })
                    }
                </div>
            </div>
        );
}

export default ActorManagerList;
