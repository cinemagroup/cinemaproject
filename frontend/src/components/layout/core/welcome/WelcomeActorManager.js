import useFetch from "../../../../services/useFetch";
import {useParams} from "react-router";
import './Welcome.css'
import {NavLink} from "react-router-dom";


const WelcomeActorManager = () => {

    const {actorManagerId} = useParams();
    const {data: actorManager, error, isLoading} = useFetch(`http://34.65.241.99:90/actorManagers/${actorManagerId}`)

    if (error) return <h1>Errore del server</h1>
    if (isLoading) return <h1>Loading..</h1>

    console.log(actorManager)
    console.log(actorManager.manager.actors)

    return (
        <>
            <h1>Welcome {actorManager.manager.actorManagerName} {actorManager.manager.actorManagerLastname}</h1>
            <img src={'../../../../utils/static/images/princess.jpg'} alt="actorManagerImg"/>

            {
                actorManager.manager.actors === null
                    ? <div>
                        <NavLink to={'/list/actorlist'}>
                            <h5>You don't have any actor? Find them!</h5>
                        </NavLink>
                    </div>
                    : <div>
                        {actorManager.manager.actors.map((actor) =>
                            <h5> {actor.actorName} {actor.actorSurname} </h5>
                        )}

                    </div>
            }


        </>
    )
}

export default WelcomeActorManager;







