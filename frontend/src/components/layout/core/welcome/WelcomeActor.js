import useFetch from "../../../../services/useFetch";
import {useParams} from "react-router";
import {useEffect} from "react";
import './Welcome.css'
import {NavLink} from "react-router-dom";


const WelcomeActor = () => {

    const {actorId} = useParams();


    const {data: actor, error, isLoading} = useFetch(`http://34.65.241.99:90/actors/${actorId}`)


   // const actorManagerId =  actor.actor.actorManager.actorManagerId;
   // const {data: actorManager, error: errorManager, isLoading: isLoadingManager} = useFetch(`http://localhost:8081/actormanagers/${actorManagerId}`)


    useEffect( () => {
        console.log(actor)
    }, [actor])

    if (error) return <h1>Errore del server</h1>
    if (isLoading) return <h1>Loading..</h1>

   // let manager= actor.actor.actorManager.actorManagerId
   /* actor.actor.actorManager === null
        ? manager = "Find a manager"
        : manager = `${actor.actor.actorManager.actorManagerName} ${actor.actor.actorManager.actorManagerLastname}`;
*/
    return (
        <>
            <h1>Welcome {actor.actor.actorName} {actor.actor.actorSurname}</h1>
            <img src={'../../../../utils/static/images/bojack.jpg'} alt="actorImg"/>
            <h4>Birthdate: {actor.actor.actorBirthdate}</h4>
            <h4>Payday: {actor.actor.actorPayday}</h4>

            {actor.actor.actorManager === null
            ? <div>
                   <NavLink to={'/list/actormanagerlist'}>
                       <h5>Don't you have a manager yet? Find him!</h5>
                   </NavLink>
              </div>
            : <div>
                    <h4>Manager: {actor.actor.actorManager.actorManagerName} {actor.actor.actorManager.actorManagerLastname}</h4>
              </div>
            }
            
        </>
    )
}

export default WelcomeActor;
