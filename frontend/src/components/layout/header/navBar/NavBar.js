import './NavBar.css'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFilm } from "@fortawesome/free-solid-svg-icons";
import {NavLink} from "react-router-dom";

const NavBar = () => {

    return (
        <div>
            <nav id={"navBar"} className="navbar navbar-expand-lg navbar-light bg-light">
                <div className="container-fluid">

                    <a id={"headerLogo"} href="#"><FontAwesomeIcon icon={ faFilm } /></a>

                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                            data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                            <li className="nav-item">
                                <NavLink to={'/'}>Home</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to={'/login'}>Login</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to={'/signup'}>Sign up</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to={'/welcome'}>Welcome</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to={'/list/actorlist'}>Actor List</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to={'/list/actormanagerlist'}>Manager List</NavLink>
                            </li>
                        </ul>
                        <form className="d-flex">
                            <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search" />
                                <button className="btn btn-primary" type="submit">Search</button>
                        </form>
                    </div>
                </div>
            </nav>
        </div>
    );
}

export default NavBar;