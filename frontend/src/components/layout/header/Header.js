import './Header.css'
import HeaderImage from "./headerImage/HeaderImage";
import NavBar from "./navBar/NavBar";

const Header = () => {

    return (
        <div>
            <NavBar />
            <HeaderImage />
        </div>
    );
}

export default Header;