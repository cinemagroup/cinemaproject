const emptyNewActorModel = {
    actorName: '',
    actorSurname: '',
    actorEmail: '',
    actorPassword: '',
    actorPayday: 0,
    actorBirthdate: ''

}

export default emptyNewActorModel;