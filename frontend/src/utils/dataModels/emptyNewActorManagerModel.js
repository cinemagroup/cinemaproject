const emptyNewActorManagerModel = {
    actorManagerName: '',
    actorManagerLastname: '',
    actorManagerEmail: '',
    actorManagerPassword: '',
}

export default emptyNewActorManagerModel;