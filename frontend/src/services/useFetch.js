import {useEffect, useState} from "react";

const useFetch = (url) => {

    const [data, setData] = useState([]);
    const [error, setError] = useState(null);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        async function init() {
            try {
                const response = await fetch(url);
                if (response.ok) {
                    const json = await response.json();
                    console.log(json);
                    setData(json);
                } else {
                    throw response;
                }
            } catch (error) {
                setError(error);
                console.error(`Error during fetch ${error}`)
            } finally {
                setIsLoading(false);
            }
        }
        init();
    }, [])

    return {data, error, isLoading};
}

export default useFetch;