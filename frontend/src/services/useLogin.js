import {useEffect, useState} from "react";

const useLogin = (url, userCredentials) => {

    const [data, setData] = useState([]);
    const [error, setError] = useState(null);
    const [isLoading, setIsLoading] = useState(true);

    /*async function retrieveToken(url,data) {
        let response = await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)

        });
        if(!response.ok){
            console.error();
        }

        return response.json();
    }*/

    useEffect(() => {
        async function init() {
            try {
                const response = await fetch(url, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(userCredentials)
                })
                if (response.ok) {
                    const json = await response.json();
                    console.log(json);
                    setData(json);
                } else {
                    throw response;
                }
            } catch (error) {
                setError(error);
                console.error(`Error during fetch ${error}`)
            } finally {
                setIsLoading(false);
            }
        }
        init();
    }, [])

    return {data, error, isLoading};
}

export default useLogin;