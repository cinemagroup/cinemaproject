

export async function signUpActor(actorData) {
    return fetch(('http://34.65.241.99:90/actors'), {
        method: 'POST',
        headers: {
            'Content-Type' : 'application/json'
        },
        body: JSON.stringify(actorData)
    })
}
