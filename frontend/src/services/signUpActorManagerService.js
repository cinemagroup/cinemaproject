

export async function signUpActorManager(actorManagerData) {
    return fetch(('http://34.65.241.99:90/actorManagers'), {
        method: 'POST',
        headers: {
            'Content-Type' : 'application/json'
        },
        body: JSON.stringify(actorManagerData)
    })
}
